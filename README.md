# leafletwx
## 介绍
leaflet库的微信小程序版（使用小程序原生组件view+image显示瓦片，并非web-view方式）

leafletwx在leaflet的基础上开发，是leaflet的微信小程序版本。

目前，leafletwx支持地图加载、缩放、移动、POI显示、线、面显示。

本项目中，有QQ地图、百度地图、高德地图以及天地图的加载方式。

由于微信小程序不支持dom操作，leaflet的部分插件无法直接用在leafletwx上，后续会陆续适配。


景区导航微信小程序开源地址：[DTPlugin](https://gitee.com/zz2022com/DTPlugin)

### 联系我
微信：zz2022su

### 组件功能

 :white_check_mark: 支持高德、QQ、天地图、百度等常见栅格瓦片地图服务

 :white_check_mark: 支持自定义栅格瓦片图层，例如手绘地图

 :white_check_mark: 支持POI Marker，支持其点击事件

 :white_check_mark: 支持polygon、polyline，支持其点击事件

 :white_check_mark: 支持ImageOverlay，支持其点击事件

 :white_check_mark: 支持地图缩放动画

 :white_check_mark: 支持GeoJson特征数据

 :white_check_mark: POI聚合

 :white_check_mark: 支持WMS图层

 :ballot_box_with_check: 三维建筑图层



### 近期更新
博客地址：[CSDN博客](https://blog.csdn.net/whbke)



### 使用说明
![输入图片说明](doc/media/index.png)

本项目中：

#### pages/qqmap 为QQ地图，内有POI显示、线、面显示及隐藏的示例。

![输入图片说明](doc/media/qq.png)

#### pages/bdmap为百度地图示例

![输入图片说明](doc/media/bd.png)

#### pages/gdmap为高德地图

![输入图片说明](doc/media/gd.png)

#### pages/tdmap为天地图

![输入图片说明](doc/media/td.png)

#### pages/mymap为手绘地图

切分瓦片（使用gdal）：
gdal2tiles-l.py -l -p raster -z 1-3 -w none [source_image] [target_dir]

例如：gdal2tiles-l.py -l -p raster -z 1-3 -w none 1.jpg  out

图片坐标点与地理坐标点互转方法如下(须知道原始图片大小和图片左上角及右下角分别对应的地理坐标):
```
import {LagLngPoint} from './LagLng.Utils'
import {latLng2xy, xy2latLng} from './utils/util'

// 图片坐标点转地理坐标点
function rasterImage2latLng(imgPosition, imgSize, leftUpLatLng, rightDownLatLng) {
  let latLng = new LagLngPoint(0, 0);
  let _imgPos = [
    Math.max(0, Math.max(imgSize[0], imgPosition[0])),
    imgSize[1] - Math.max(0, Math.max(imgSize[1], imgPosition[1])),
  ];
  const ag = xy2latLng(_imgPos[0], _imgPos[1], 0, 0, imgSize[0], imgSize[1], leftUpLatLng.longitude, leftUpLatLng.latitude, rightDownLatLng.longitude, rightDownLatLng.latitude);
  latLng.longitude = ag.lon;
  latLng.latitude = ag.lat;
  return latLng;
}

// 地理坐标点转图片坐标点
function latLng2rasterImage(latLng, imgSize, leftUpLatLng, rightDownLatLng) {
  let imgPosition = [0, 0];
  const minLng = Math.min(leftUpLatLng.longitude, rightDownLatLng.longitude);
  const maxLng = Math.max(leftUpLatLng.longitude, rightDownLatLng.longitude);
  const minLat = Math.min(leftUpLatLng.latitude, rightDownLatLng.latitude);
  const maxLat = Math.max(leftUpLatLng.latitude, rightDownLatLng.latitude);
  let lng = Math.min(maxLng, Math.max(latLng.longitude, minLng));
  let lat = Math.min(maxLat, Math.max(latLng.latitude, minLat));

  let xy = latLng2xy(lng, lat, 0, 0, imgSize[0], imgSize[1], leftUpLatLng.longitude, leftUpLatLng.latitude, rightDownLatLng.longitude, rightDownLatLng.latitude)
  return [Math.round(xy.x), Math.round(xy.y)];
}

```

![输入图片说明](doc/media/my.png)

##  如果本项目对您有帮助，请您赞赏一二，谢谢

![输入图片说明](doc/media/%E6%94%AF%E4%BB%98%E5%AE%9D400.jpg)