export const defaultIcons = {
    location: "./assets/icon/location.png",
    locationNow: "./assets/icon/location-now.png",
    compass: "./assets/icon/compass.png",
    zoomin: "./assets/icon/zoomin.png",
    zoomout: "./assets/icon/zoomout.png"
}

export const zhgeoCfg = {
    size: {
        width: wx.getSystemInfoSync().windowWidth,
        height: wx.getSystemInfoSync().windowHeight
    },
    retina: true,
    retinaMapZoomStep: 1,
    dragTimeTolerance: 30,  // ms
    dragPixelTolerance: 10, // px
}
