// pages/gdmap/gdmap.js
var L = require('../../components/zhgeo/leafletwx')
import {createMap} from '../../components/zhgeo/base.map'

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const container = this.selectComponent('#gd-leafletwx')
    let min_zoom = 10
    let max_zoom = 18
    createMap(container,  {
        minZoom: min_zoom,
        maxZoom: max_zoom,
      }, function(map) {

      var geoJsonData = {
        "type": "FeatureCollection", 
        "features": [
          { "type": "Feature", "id":"1", "properties": { "address": "2"   }, "geometry": { "type": "Point", "coordinates": [120.625348,31.294888] } },
          { "type": "Feature", "id":"2", "properties": { "address": "151" }, "geometry": { "type": "Point", "coordinates": [120.625675,31.294898] } },
          { "type": "Feature", "id":"3", "properties": { "address": "21"  }, "geometry": { "type": "Point", "coordinates": [120.625375,31.294636] } },
          { "type": "Feature", "id":"4", "properties": { "address": "14"  }, "geometry": { "type": "Point", "coordinates": [120.626067,31.294609] } },
          { "type": "Feature", "id":"5", "properties": { "address": "38B" }, "geometry": { "type": "Point", "coordinates": [120.627379,31.296556] } },
          { "type": "Feature", "id":"6", "properties": { "address": "38"  }, "geometry": { "type": "Point", "coordinates": [120.623645,31.293604] } },
          { "type": "Feature", "id":"7", "properties": { "address": "39"  }, "geometry": { "type": "Point", "coordinates": [120.626008,31.294435] } },
          { "type": "Feature", "id":"8", "properties": { "address": "40"  }, "geometry": { "type": "Point", "coordinates": [120.625680,31.294315] } }
        ]
      };
      L.tileLayer('http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}', { subdomains: "1234" }).addTo(map)
      
      var markers = L.markerClusterGroup();
      
      var geoJsonLayer = L.geoJson(geoJsonData, {
        src: 'https://unpkg.com/leaflet@1.9.4/dist/images/marker-icon-2x.png',
        onEachFeature: function (feature, layer) {
          layer.bindPopup(feature.properties.address);
        }
      })
      markers.addLayer(geoJsonLayer);

      map.addLayer(markers);
      map.fitBounds(markers.getBounds());
      
      markers.on('clusterclick', function (a) {
        // a.layer is actually a cluster
        console.log('cluster ' + a.layer.getAllChildMarkers().length);
      });
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})