class LevelLayer {
  constructor(index, zIndex, name) {
    this.index = index;
    this.zIndex = zIndex;
    this.name = name;
    this.top = 0;
    this.left = 0;
    this.elemid = 'leaflet-tile-container-' + name
  }
}

class LevelManager {
  constructor(layerLeafletId) {
    this.layerLeafletId = layerLeafletId
    this.usingIndex = 0
    this.levelArray = [
      new LevelLayer(0, 0, `${layerLeafletId}-0`),
      new LevelLayer(1, 1, `${layerLeafletId}-1`)
    ]

    this.getShowingLevel = function() {
      return this.levelArray[this.usingIndex]
    }

    this.getHidingLevel = function() {
      return this.levelArray[(this.usingIndex + 1) % 2]
    }
  
    this.getShowingIndex = function() {
      return this.usingIndex;
    }
  
    this.getHidingIndex = function() {
      return (this.usingIndex + 1) % 2
    }
  
    this.toggle = function() {
      this.levelArray[this.usingIndex].zIndex = 0;
      this.usingIndex = (this.usingIndex + 1) % 2
      this.levelArray[this.usingIndex].zIndex = 1;
    }
  }
}

class CanvasImageLayer {
  constructor(index, zIndex, name) {
    this.index = index;
    this.zIndex = zIndex;
    this.name = name;
    this.top = 0;
    this.left = 0;
    this.width = 0;
    this.height = 0;
    this.show = false;
    this.elemid = 'leaflet-canvas-overlay-' + index
  }
}


class CanvasImageManager {
  constructor(imageLeafletId) {
    this.imageLeafletId = imageLeafletId
    this.usingIndex = 0
    this.imageArray = [
      new CanvasImageLayer(0, 0, `${imageLeafletId}-0`),
      new CanvasImageLayer(1, 1, `${imageLeafletId}-1`)
    ]

    this.getShowingImage = function() {
      return this.imageArray[this.usingIndex]
    }

    this.getHidingImage = function() {
      return this.imageArray[(this.usingIndex + 1) % 2]
    }
  
    this.getShowingIndex = function() {
      return this.usingIndex;
    }
  
    this.getHidingIndex = function() {
      return (this.usingIndex + 1) % 2
    }
  
    this.toggle = function() {
      this.imageArray[this.usingIndex].zIndex = 30;
      this.imageArray[this.usingIndex].show = false;
      this.usingIndex = (this.usingIndex + 1) % 2
      this.imageArray[this.usingIndex].zIndex = 31;
      this.imageArray[this.usingIndex].show = true;
    }
  }
}

function layerLeafletId2name (leafletId) {
  return `layer${leafletId}`
}


function canvasImageId2name (leafletId) {
  return `canvasImage${leafletId}`
}

export {
  layerLeafletId2name, canvasImageId2name,
  LevelManager, CanvasImageManager
}