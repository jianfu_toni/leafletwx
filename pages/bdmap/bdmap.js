// pages/bdmap/bdmap.js
var L = require('../../components/zhgeo/leafletwx')
require('./tileLayer.baidu')
import {createMap} from '../../components/zhgeo/base.map'

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const container = this.selectComponent('#bd-leafletwx')
    createMap(container, {
        crs: L.CRS.Baidu,
        minZoom: 3,
        maxZoom: 18,
        zoom: 18
      }, function(map) {
      L.tileLayer.baidu({ layer: 'vec' }).addTo(map),
      map.setView([31.300635, 120.632314]);
      
      var geojsonFeature = [
        {
          "type": "Feature",
          "properties": {
              "name": "Coors Field",
          },
          "geometry": {
              "type": "Point",
              "coordinates": [120.632314, 31.300635]
          }
        },
        {
          "type": "Feature",
          "properties": {
              "name": "LineString Field",
              "color": "red"
          },
          "geometry": {
              "type": "LineString",
              "coordinates": [[120.631591,31.301028], [120.63178,31.301032]]
          }
        },
        {
          "type": "Feature",
          "properties": {
              "name": "Polygon Field",
              "color": "blue"
          },
          "geometry": {
              "type": "Polygon",
              "coordinates": [[[120.632207,31.301101], [120.632719,31.301066],[120.632741,31.300916],[120.632278,31.300958],[120.632207,31.301101]]]
          }
        },
        {
          "type": "Feature",
          "properties": {
              "name": "MultiPoint Field",
          },
          "geometry": {
              "type": "MultiPoint",
              "coordinates": [[120.632207,31.301101], [120.632719,31.301066],[120.632741,31.300916],[120.632278,31.300958]]
          }
        }
      ];
      
      var gj = L.geoJSON(geojsonFeature, 
        {
          src: 'https://unpkg.com/leaflet@1.9.4/dist/images/marker-icon-2x.png',
          style: (feature) => {
            return {
              color: feature.properties.color,
            }
          },
          onEachFeature: function (feature, layer) {
            layer.bindPopup(feature.properties.name);
          }
        }).addTo(map)
      
      // 删除geojson图层
      // map.removeLayer( gj );
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})