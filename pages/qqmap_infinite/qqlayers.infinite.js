var L = require('../../components/zhgeo/leafletwx')

L.TileLayer.InfiniteTxMapTileLayer = L.TileLayer.extend({
  options: {
    minNativeZoom: 0
  },

  initialize: function (urlTemplate, options) {
    L.TileLayer.prototype.initialize.call(this, urlTemplate, options);
  },

  createTile: function (coords, done) {
    var tile = L.TileLayer.prototype.createTile.call(this, coords, done);
    tile._originalCoords = coords;
    tile._originalSrc = tile.src;

    return tile;
  },

  _clampZoom: function (zoom) {
    var options = this.options;
    if (undefined !== options.minNativeZoom && zoom < options.minNativeZoom) {
      return options.minNativeZoom;
    }  
    return zoom;
  },

  _addTile: function (coords) {
    var tile = L.TileLayer.prototype._addTile.call(this, coords);
    var options = this.options;
    var zoom = coords.z;
    if (undefined !== options.maxNativeZoom && options.maxNativeZoom < zoom) {
      return this._tileOutError(tile);
    }
    return tile;
  },

  _createCurrentCoords: function (originalCoords) {
    var currentCoords = this._wrapCoords(originalCoords);

    currentCoords.fallback = true;

    return currentCoords;
  },

  _tileOutError: function (tile) {
    var options = this.options;
    var layer = this, // `this` is bound to the Tile Layer in L.TileLayer.prototype.createTile.
        originalCoords = tile._originalCoords,
        currentCoords = tile._currentCoords = tile._currentCoords || layer._createCurrentCoords(originalCoords),
        fallbackZoom = options.maxNativeZoom,
        scale = Math.pow(2, (originalCoords.z - options.maxNativeZoom)),
        tileSize = layer.getTileSize(),
        newUrl, top, left;

    // Modify tilePoint for replacement img.
    currentCoords.z = fallbackZoom;
    currentCoords.x = Math.floor(currentCoords.x / scale);
    currentCoords.y = Math.floor(currentCoords.y / scale);

    // Generate new src path.
    newUrl = layer.getTileUrl(currentCoords);

    // Zoom replacement img.
    tile.width = (tileSize.x * scale) + 'px';
    tile.height = (tileSize.y * scale) + 'px';

    // Compute margins to adjust position.
    top = (originalCoords.y - currentCoords.y * scale) * tileSize.y;
    tile.top = (tile.top - top);
    left = (originalCoords.x - currentCoords.x * scale) * tileSize.x;
    tile.left = (tile.left - left);

    // Crop (clip) image.
    // `clip` is deprecated, but browsers support for `clip-path: inset()` is far behind.
    // http://caniuse.com/#feat=css-clip-path
    // tile.clip = 'rect(' + top + 'px ' + (left + tileSize.x) + 'px ' + (top + tileSize.y) + 'px ' + left + 'px)';
    tile.src = newUrl;
    return tile;
  },

  getTileUrl: function (tilePoint) {
      var urlArgs,
          getUrlArgs = this.options.getUrlArgs;

      // tilePoint.z = this._getZoomForUrl()
      if (getUrlArgs) {
          var urlArgs = getUrlArgs(tilePoint);
      } else {
          urlArgs = {
              z: tilePoint.z,
              x: tilePoint.x,
              y: tilePoint.y,
              s: this._getSubdomain(tilePoint)
          };
      }
      if (this._map && !this._map.options.crs.infinite) {
        var invertedY = this._globalTileRange.max.y - tilePoint.y;
        if (this.options.tms) {
          urlArgs['y'] = invertedY;
        }
        urlArgs['-y'] = invertedY;
      }
      return L.Util.template(this._url, L.extend(urlArgs, this.options));
  }
});

L.tileLayer.infiniteTxMapTileLayer = function (type, options) {
  var getUrlArgs, url, subdomain = '012';
  if (type == 'Normal') { //地图带路线轮廓 有轮廓/有路线
      url = 'http://rt1.map.gtimg.com/realtimerender/?z={z}&x={x}&y={y}&type=vector&style=1&v=1.1.1'; //ok //普通地图
      getUrlArgs = function (tilePoint) {
          return {
              //地图
              z: tilePoint.z,
              x: tilePoint.x,
              y: Math.pow(2, tilePoint.z) - 1 - tilePoint.y
          };
      }

  } else if (type == 'Satellite') { //卫星图,无轮廓/无路线

      url = 'http://p3.map.gtimg.com/sateTiles/{z}/{x}/{y}/{G}_{H}.jpg?version=229';
      getUrlArgs = function (tilePoint) {
          return {
              //卫星图
              z: tilePoint.z,
              x: Math.floor(tilePoint.x / 16),
              y: Math.floor((Math.pow(2, tilePoint.z) - 1 - tilePoint.y) / 16),
              G: tilePoint.x,
              H: Math.pow(2, tilePoint.z) - 1 - tilePoint.y
          };
      }


  }  else if (type=='Landform') {//地形图,有轮廓/有路线
      url = 'http://rt1.map.gtimg.com/tile?z={z}&x={x}&y={y}&type=vector&styleid=3&version=263' //ok //地形地图
      getUrlArgs = function (tilePoint) {
          return {
              //地形图
              z: tilePoint.z,
              x: tilePoint.x,
              y: Math.pow(2, tilePoint.z) - 1 - tilePoint.y
          };
      }
  }
  options = L.extend(options??{}, {
      subdomain:'012',
      getUrlArgs:getUrlArgs,
  })
  return new L.TileLayer.InfiniteTxMapTileLayer(url, options);
};