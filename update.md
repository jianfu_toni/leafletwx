2023.09.18

1.修复tileLayer无法删除的问题

2.支持多层tile layer

3.支持wms图层


2023.09.14

1.缓解canvas移动、变更时频繁闪动的问题


2023.09.03

1.新增POI点聚合功能

2.合并短时间内的polygon、polyline等canvas元素渲染操作，以减少渲染次数，提升性能

3.问题：Vue3的Proxy兼容有问题，目前可以使用"@vue/reactivity"的"toRaw"方法，直接使用Proxy中的target


2023.08.22

1.修复双击最大层级地图是，ImageOverlay图层消失的问题

2.在uniapp中，兼容Vue2

3.增加对GeoJson的支持


2023.08.14

1.修复IOS下polygon崩溃问题

2.增加缩放地图动画

3.为polygon、polyline等canvas组件增加点击事件

4.增加对ImageOverlay的支持

5.地图的缩放控件增加缩放极限的提醒（按钮变灰提醒）


2023.04.20

1.修正marker popup存在的问题，示例增加在qqmap中

2.新增手绘地图的使用示例，使用leaflet的rastercoords插件。
