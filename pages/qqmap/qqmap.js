// pages/qqmap/qqmap.js
var L = require('../../components/zhgeo/leafletwx')
require('./qqlayers')
import {createMap} from '../../components/zhgeo/base.map'
import {defaultIcons} from '../../components/zhgeo/config.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  showLocation() {
    console.log('showLoction')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const container = this.selectComponent('#qq-leafletwx')
    let min_zoom = 10
    let max_zoom = 18
    let that = this
    createMap(container,  {
        minZoom: min_zoom,
        maxZoom: max_zoom,
      }, function(map) {
      wx.leaflet.showLocation = that.showLocation.bind(that);
      
      console.log(map)
      var Normal = L.tileLayer.txMapTileLayer("Normal", /*{detectRetina: true}*/).addTo(map); //调用 腾讯地图
      map.setView([31.294516,120.625814], 17);
      // 限定地图范围
      var corner1 = L.latLng(31.29571,120.623941),
      corner2 = L.latLng(31.289586,120.63455),
      bounds = L.latLngBounds(corner1, corner2);
      // map.setMaxBounds(bounds);

      // let wmsLayer = L.tileLayer.wms('http://ows.mundialis.de/services/service?', {
      //       layers: 'TOPO-WMS,OSM-Overlay-WMS',
      //   }).addTo(map);
      var Landform = L.tileLayer.txMapTileLayer("Satellite", {detectRetina: true}).addTo(map); //调用 腾讯地图

      // 添加marker
      let m = L.marker([31.2948516,120.625814], {
        src: defaultIcons.locationNow,
        width: 32,
        height: 32,
        showInCenter: false,
      }).addTo(map);
      // 删除marer
      // map.removeLayer( m );

      // popup      
      m.bindPopup("这是个弹框", {
        width: 60,
      });

      // 添加路线      
      let l = L.polyline([[31.293516,120.625814], [31.294516,120.625214], [31.294516,120.626114]], {color: '#ff0000', weight: 3}).addTo(map)
        .on({
          click: (e) => {
            console.log(e)
          }
        });
      // 删除线路
      // map.removeLayer( l );

      L.circle([31.293516,120.625814],50).addTo(map)

      // 添加区域, 并监听点击事件
      let y = L.polygon([[31.2930516,120.625814], [31.2940516,120.625214], [31.2940516,120.626114]], {color: '#ff0000', weight: 4}).addTo(map)
        .on({
          click: (e) => {
            console.log(e)
          }
        });
      // 删除区域
      // map.removeLayer( y );

      
      m = L.marker([31.2944516,120.625814], {
        src: defaultIcons.location,
        width: 32,
        height: 32,
        title: '沧浪亭',
        showFooter: true,
        footerWidth: 60,
        footerPosition: 1,  // 0：左 1:右 2：上 3：下
        left_badge: '左', //  为空时不显示
        right_badge: '右',  // 为空时不显示
        showInCenter: true, // ture: 图标中心在POI中心 false：图标下边缘中心位于POI中心
      }).addTo(map);
      // 删除线路
      // map.removeLayer( l );

      
      var corner1 = L.latLng(31.294945,120.625059),
      corner2 = L.latLng(31.294726,120.625576),
      bounds = L.latLngBounds(corner1, corner2);
      var url = 'https://hbimg.huaban.com/c939311c7b988a0d77fa7260248a383416ef488d36dd1-G5tt78_fw236';
      var image = L.imageOverlay(url, bounds,{interactive:true}).addTo(map)
        .on({
          click: (e) => {
            console.log(e)
          }
        });
      let _layers = map._layers;
      for (let i in _layers) {
        // _layers[i].remove();
        // map.removeLayer(_layers[i])
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})