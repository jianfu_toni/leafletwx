// pages/tdmap/tdmap.js
var L = require('../../components/zhgeo/leafletwx')
import {createMap} from '../../components/zhgeo/base.map'
import {defaultIcons} from '../../components/zhgeo/config.js';

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const container = this.selectComponent('#td-leafletwx')
    let min_zoom = 10
    let max_zoom = 18
    createMap(container,  {
        minZoom: min_zoom,
        maxZoom: max_zoom,
      }, function(map) {
      L.tileLayer('http://t{s}.tianditu.gov.cn/vec_w/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER=vec&STYLE=default&TILEMATRIXSET=w&FORMAT=tiles&TILECOL={x}&TILEROW={y}&TILEMATRIX={z}&tk=56b81006f361f6406d0e940d2f89a39c', { subdomains: ['0', '1', '2', '3', '4', '5', '6', '7'] }).addTo(map)
      
      map.setView([31.2969292,120.6212541], 18);
      // 添加marker
      let m = L.marker([31.2969292,120.6212541], {
        src: defaultIcons.locationNow,
        width: 32,
        height: 32,
        showInCenter: false,
      }).addTo(map);
      // 删除marer
      // map.removeLayer( m );
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})