// pages/mymap/mymap.js
var L = require('../../components/zhgeo/leafletwx')
import {createMap} from '../../components/zhgeo/base.map'
import {defaultIcons} from '../../components/zhgeo/config.js';
L.RasterCoords = require('./rastercoords')

Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const container = this.selectComponent('#my-leafletwx')
    let min_zoom = 1
    let max_zoom = 3
    let zoom = 1
    let img_width = 3000;   // 原始图片宽度
    let img_height = 2216;  // 原始图片高度
    createMap(container,   {
        crs: L.CRS.Simple,
        minZoom: min_zoom,
        maxZoom: max_zoom,
      }, function(map) {
      let img = [
          img_width, // original width of image
          img_height  // original height of image
      ];
      let rc = new L.RasterCoords(map, img);
      map.setView(rc.unproject([img_width / 2, img_height / 2]), zoom);
      L.tileLayer('https://map.zz2022.com/media/tiles/out/{z}/{x}/{y}.png', {
        noWrap: true,
        bounds: rc.getMaxBounds(),        
        detectRetina: true,
        // maxNativeZoom: that.rc.zoomLevel()
      }).addTo(map)

      // 添加marker
      let m = L.marker(rc.unproject([img_width * 0.2, img_height * 0.2]), {
        src: defaultIcons.locationNow,
        width: 32,
        height: 32,
        showInCenter: false,
      }).addTo(map);
      m = L.marker(rc.unproject([img_width * 0.8, img_height * 0.8]), {
        src: defaultIcons.locationNow,
        width: 32,
        height: 32,
        showInCenter: false,
      }).addTo(map);
      // 删除marer
      // map.removeLayer( m );
      var latlngs = [[img_width * 0.2, img_height * 0.2], [img_width * 0.8, img_height * 0.8]]
      for (var i=0; i<latlngs.length; ++i) {
        latlngs[i] = rc.unproject(latlngs[i]);
      }
      var path = L.polyline(latlngs, {
        color: '#ff0000',
        weight: 3,
        dashArray: "15,15",
        // dashSpeed: -30
      }).addTo(map);
  
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})