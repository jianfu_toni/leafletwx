// index.js

// 获取应用实例
const app = getApp()

Page({
  data: {
  },
  onLoad() {
  },
  navToQQMap() {    
    wx.navigateTo({
      url: "/pages/qqmap/qqmap"
    })
  },
  navToBDMap() {    
    wx.navigateTo({
      url: "/pages/bdmap/bdmap"
    })
  },
  navToGDMap() {    
    wx.navigateTo({
      url: "/pages/gdmap/gdmap"
    })
  },
  navToTDMap() {    
    wx.navigateTo({
      url: "/pages/tdmap/tdmap"
    })
  },
  navToMYMap() {    
    wx.navigateTo({
      url: "/pages/mymap/mymap"
    })
  },
  navToQQMapInfinite() {
    wx.navigateTo({
      url: "/pages/qqmap_infinite/qqmap_infinite"
    })
  }
})
