var L = require('./leafletwx')
import {zhgeoCfg} from "./config"

const wx2leflet = {
  'tap': 'click',
  // 'contextmenu',
  'doubleTap': 'dblclick',
  // 'keypress',
  'touchstart': 'mousedown',
  'touchmove': 'mousemove',
  // 'mouseout',
  // 'mouseover',
  'touchend': 'mouseup',
  // 'touchcancel': 'pointercancel',
  // 'touchstart': 'pointerdown',
  // 'touchmove': 'pointermove',
  // 'touchend': 'pointerup',
    'pinch': 'pinch',
  // 'touchend',
  // 'touchstart'
};

let lastTouchMoveEvent;
function filterTouchMoveEvent(e) {
  if (e.wxtype == 'touchstart') {
    lastTouchMoveEvent = e;
    return e;
  } else if (e.wxtype == 'touchmove') {
    if (lastTouchMoveEvent == null || e.touches == null || e.touches.length < 1) {
        return e;
    }
    var current = e.touches[0];
    var last = lastTouchMoveEvent.touches[0];
    if (e.timeStamp - lastTouchMoveEvent.timeStamp >= zhgeoCfg.dragTimeTolerance) {
      lastTouchMoveEvent = e;
      return e;
    }
    const offsetX = current.clientX - last.clientX;
    const offsetY = current.clientY - last.clientY;
    if (Math.sqrt(offsetX * offsetX + offsetY * offsetY) >= zhgeoCfg.dragPixelTolerance) {
      lastTouchMoveEvent = e;
      return e;
    }
    return null;
  } else if (e.wxtype == 'touchend') {
    lastTouchMoveEvent = null;
    return e;
  } else {
    return e;
  }
}


function addEventListener(obj, event, func, useCapture) {
  wx.leaflet.L.stamp(func);
  obj.eventHeaders ??= {};
  obj.eventHeaders[event] ??= [];
  let headers = obj.eventHeaders[event];
  headers ??= [];
  headers.push({
      "event": event,
      "func": func,
      "useCapture": useCapture
  });
//   console.log("addEventListener, event: " + event + ", useCapture: " + useCapture);
}

function removeEventListener(obj, event, func, useCapture) {
  obj.eventHeaders[event] ??= [];
  let headers = obj.eventHeaders[event];
  let found = false;
  for (let i=0; i<headers.length; ++i) {
    let header = headers[i];
    if (header.func._leaflet_id == func._leaflet_id) {
        headers.splice(i, 1);
        found = true;
        break;
    }
  }
//   console.log("removeEventListener, event: " + event + ", useCapture: " + useCapture
//     + ", " + (found?"found":"not found"));
}


function processMarkerEvent(event) {
  if (event.type === "tap") {
      var icon = wx.leaflet.id2obj[event.currentTarget.dataset.iconid];
      var marker = wx.leaflet.id2obj[icon.markerid];
      marker.fire('click');
      if (icon.isClusterMarker && marker._group) {
        marker._group.fire('click', {layer: marker}, true);
      }
  }
}

  
function processImageOverlayEvent (event) {
  var that = wx.leaflet.id2obj[wx.leaflet.id2obj[event.currentTarget.dataset.lid].overlayid];
  // console.log(event);
  cvtEventType(event);
  if (event.type == null) return;
  that.eventHeaders ??= {};
  event = filterTouchMoveEvent(event);
  if (event == null) return;
  that.fire(event.type)
}

function cvtEventType(event) {    
  let type = wx2leflet[event.type];
  event.wxtype = event.type;
  event.type = type;
}

var WxDomUtils = L.Class.extend({
  initContainer(container) {
    this._container = container;
  },

  removeTileLayer: function(layerName) {
    var that = this._container;
    that.setData({[`tileLayers.${layerName}`]: null})
  },
  
  addTiles: function(layerName, zoom, tiles, replace) {
    var that = this._container;
    var levelManager = that.data.tileLayers[layerName];
    if (levelManager == null) return;
    // 是否是初始化
    var initMode = true;
    for (var i in levelManager.levelArray) {
      if (levelManager.levelArray[i].tiles !== undefined) {
        initMode = false;
      }
    }
  
    var showingLevel = levelManager.getShowingLevel()
    if (showingLevel.zoom !== zoom) {
      levelManager.toggle();
      showingLevel = levelManager.getShowingLevel()
    }
  
    if (replace === true) {
      showingLevel.tiles = {};
    } else {
      showingLevel.tiles ??= {};
    }
    showingLevel.zoom = zoom
    for (let i=0; i<tiles.length; ++i) {
      showingLevel.tiles[tiles[i]._leaflet_id] = tiles[i];
    }
  
    if (initMode) {    
      that.setData({[`tileLayers.${layerName}`]: levelManager})
      return
    }
  
    that.setData({
      [`tileLayers.${layerName}.levelArray[${levelManager.getHidingIndex()}].zIndex`]: levelManager.getHidingLevel().zIndex,
      [`tileLayers.${layerName}.levelArray[${levelManager.getShowingIndex()}].zIndex`]: showingLevel.zIndex,
      [`tileLayers.${layerName}.levelArray[${levelManager.getShowingIndex()}].tiles`]: showingLevel.tiles
    }, function() {
      let _levelManager = that.data.tileLayers[layerName];
      if (_levelManager == null) return;
      let hidingLevel = _levelManager.getHidingLevel();
      if (hidingLevel.tiles !== undefined && Object.keys(hidingLevel.tiles).length > 0) {
        // hide
        hidingLevel.tiles = {}
        this.setData({
          [`tileLayers.${layerName}.levelArray[${_levelManager.getHidingIndex()}].tiles`]: hidingLevel.tiles
        })
      }
    })
  },
  
  setMarkers: function() {
    var that = this._container;
    that.setData(that._waitingMarkers);
    that._waitingMarkers = null;
    that._markerRequest = null;
  },
  
  removeMarker: function(icon) {
    var that = this._container;
    that._waitingMarkers ??= {};
    that._waitingMarkers['markers.' + icon.unique] = null
    that._markerRequest = that._markerRequest || L.Util.requestAnimFrame(this.setMarkers.bind(this))
  },
  
  updateMarkers: function(icon) {
    var that = this._container;
    icon.unique = 'icon_' + icon._leaflet_id
    that._waitingMarkers ??= {};
    that._waitingMarkers['markers.' + icon.unique] = icon
    that._markerRequest = that._markerRequest || L.Util.requestAnimFrame(this.setMarkers.bind(this))
  },

  updatePopups: function(popup) {
    var that = this._container;
    popup.unique = "pp_" + popup._leaflet_id;
    popup.show = true;
    that.setData({
        ['popups.' + popup.unique]: popup
    });
  },
  
  showPopup: function(item) {
    var that = this._container;
    var popups = that.data.popups ?? {};
    let attr = item.unique;
    if (attr in popups) {
        popups[attr].show = true;
        that.setData({
          ['popups.' + attr]: popups[attr]
        });
    }
  },
  
  removePopup: function(item) {
    var that = this._container;
    var popups = that.data.popups ?? {};
    let attr = item.unique;
    if (attr in popups) {
        popups[attr].show = false;
        that.setData({
          ['popups.' + attr]: popups[attr]
        });
    }
  },

  // TODO
  closePoiPopup: function(poi) {
    if (poi.marker_leaflet_id != null && wx.leaflet.id2obj[poi.marker_leaflet_id] != null) {
      wx.leaflet.id2obj[poi.marker_leaflet_id].closePopup();
    }
  },

  setMapPanePosition: function(point) {
    var that = this._container;
    var mapPaneLeft = Math.round(point.x)
    var mapPaneTop = Math.round(point.y)
    // var overlayCanvasLeft = -mapPaneLeft;
    // var overlayCanvasTop = -mapPaneTop;
    that.setData({ mapPaneLeft, mapPaneTop});
  },

  getMapPanePosition: function() {
    var that = this._container;
    return new L.Point(that.data.mapPaneLeft, that.data.mapPaneTop);
  },
  
  // setOverlayCanvasSize: function(width, height, callback) {
  //     var that = this._container;
  //     that.setData({
  //         overlayCanvasWidth: width,
  //         overlayCanvasHeight: height,
  //     }, callback);
  // },
  
  getOverlayCanvasSize: function() {
      var that = this._container;
      return {
          width: that.data.overlayCanvasWidth,
          height: that.data.overlayCanvasHeight,
      };
  },
  
  setOverlayCanvasPosition: function(point, callback) {
      var that = this._container;
      that.setData({
          [`overlayCanvasData.left`]: point.x,
          [`overlayCanvasData.top`]: point.y,
      }, callback);
    },
    
  getOverlayCanvasPosition: function() {
      var that = this._container;
      return new L.Point(-that.data.mapPaneLeft, -that.data.mapPaneTop);
      // return new L.Point(that.data.overlayCanvasData.left, that.data.overlayCanvasData.top);
    },
  
  updateOverlayCanvasData: function (name, data, callback) {
    var that = this._container;
    var mapPanePosition = that.wxDomUtils.getMapPanePosition();    
    var canvasImage = that.data.canvasImages[name];
    canvasImage.toggle();
    that.setData ({
      [`canvasImages.${name}.imageArray[${canvasImage.getHidingIndex()}].zIndex`]: canvasImage.getHidingImage().zIndex,
      [`canvasImages.${name}.imageArray[${canvasImage.getHidingIndex()}].data`]: '',
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].zIndex`]: canvasImage.getShowingImage().zIndex,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].width`]: that.data.overlayCanvasWidth,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].height`]: that.data.overlayCanvasHeight,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].left`]: -mapPanePosition.x,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].top`]: -mapPanePosition.y,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].data`]: data,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].show`]: canvasImage.getShowingImage().show,
      [`canvasImages.${name}.imageArray[${canvasImage.getShowingIndex()}].elemid`]: canvasImage.getShowingImage().elemid,
    }, () => {
      that.setData({
        [`canvasImages.${name}.imageArray[${canvasImage.getHidingIndex()}].show`]: canvasImage.getHidingImage().show,
      }, callback)
    })
  },
  
  // setOverlayCanvasPositionAndSize: function(width, height, point, callback) {
  //     var that = this._container;
  //     that.setData({
  //         overlayCanvasWidth: width,
  //         overlayCanvasHeight: height,
  //         overlayCanvasLeft: point.x,
  //         overlayCanvasTop: point.y,
  //     }, callback);
  // },
  
  getBoundingClientRect: function() {
    var that = this._container;
    return {
        width: that.data.mapWidth,
        height: that.data.mapHeight,
        top: that.data.mapTop,
        left: that.data.mapLeft
    }
  },
  
  getTouchEventPosition: function(e) {
    var that = this._container;
    if (e.wxtype == 'touchstart' || e.wxtype == 'touchmove') {
        var first = e.touches[0];
        return new L.Point(
            first.clientX - that.data.mapLeft,
            first.clientY - that.data.mapTop);
    } else if (e.wxtype == 'touchend') {
        var first = e.changedTouches[0];
        return new L.Point(
            first.clientX - that.data.mapLeft,
            first.clientY - that.data.mapTop);
    } else if (e.wxtype == 'doubleTap') {
      return new L.Point(
          e.changedTouches[0].clientX - that.data.mapLeft,
          e.changedTouches[0].clientY - that.data.mapTop);
    } else if (e.wxtype == null) {
      return new L.Point(
          e.clientX - that.data.mapLeft,
          e.clientY - that.data.mapTop);
    } else {
      return new L.Point(
          e.detail.x - that.data.mapLeft,
          e.detail.y - that.data.mapTop);
    }
  },
  
  
  tileLayerTransform: function(level, keyframes, duration) {
    var that = this._container;
    // let selector = '.leaflet-map-pane'
    let selector = '#' + level.elemid
    that.animate(selector, keyframes, duration ?? 0, function () {
      that.clearAnimation(selector, function () {})
    })
  },
  
  overlayCanvasTransform: function(canvasImage, keyframes, duration) {
    var that = this._container;
    let selector = '#' + canvasImage.elemid
    that.animate(selector, keyframes, duration ?? 0, function () {
      that.clearAnimation(selector, function () {})
    })
  },
  
  markerLayerTransform: function(selector, zoom, keyframes, duration) {
    var that = this._container;
    selector = selector == null ? '.leaflet-marker-item' : selector
    // console.log(keyframes)
    that.animate(selector, keyframes, duration ?? 0, function () {
      that.clearAnimation(selector, function () {})
    })
  },
  
  setZoomControlState: function(zoomInState, zoomOutState) {
      var that = this._container;
      that.setData({
          zoomInState,
          zoomOutState
      })
  },
  
  updateImageLayer: function(img) {
    var that = this._container;
    that.unique = "img_" + img._leaflet_id;
    that.setData({
      [`imageLayers.${that.unique}`]: img
    })
  },
  
  removeImageLayer: function(img) {
    var that = this._container;
    that.unique = "img_" + img._leaflet_id;
    that.setData({
      [`imageLayers.${that.unique}`]: null
    })
  },
  
  imageLayerTransform: function(img, keyframes, duration) {
    var that = this._container;
    let selector = '#leaflet-image-item-' + img._leaflet_id
    that.animate(selector, keyframes, duration ?? 0, function () {
      that.clearAnimation(selector, function () {})
    })
  },
  
  getImageLayerPosition: function(img) {
    return {
      x: img.left,
      y: img.top
    }
  },
  
  getImageLayerSize: function(img) {
    return {
      width: img.width,
      height: img.height
    }
  },

})

export {wx2leflet, filterTouchMoveEvent,
  cvtEventType, WxDomUtils,
  addEventListener, removeEventListener, 
  processMarkerEvent, processImageOverlayEvent}