var L = require('./leafletwx')
import {WxDomUtils, addEventListener, removeEventListener} from './zhgeo.dom'

/**
 * 创建地图接口
 * @param {*} container 
 * @param {*} options 
 * @param {*} callback 
 */
export function createMap(container, options, callback) {
  wx.leaflet ??= {};
  container._showingTileLayerIndex = 0;
  let wxDomUtils = new WxDomUtils();
  wxDomUtils.initContainer(container);
  container.wxDomUtils = wxDomUtils;

  var width = container.data.overlayCanvasWidth;
  var height = container.data.overlayCanvasHeight;
  container.overlayCanvas = wx.createOffscreenCanvas({type: '2d', width: width, height: height});
  options ??= {};
  options.preferCanvas = true
  let map = L.map(container, options);
  container.overlayCanvas.addEventListener = function(event, func, useCapture) {
    addEventListener(container.overlayCanvas, event, func, useCapture);
  },
  container.overlayCanvas.removeEventListener = function(event, func, useCapture) { 
    removeEventListener(container.overlayCanvas, event, func, useCapture);
  }

  if (callback != null)  callback(map);
};

